#include "cat.hh"
#include "private/cat_private.hh"

#include <fmt/format.h>

#include <array>
#include <cstdlib>
#include <cstdio>

CMAKE_PROJECT_TEMPLATE_BEGIN_PRIVATE_NAMESPACE

CatPrivate::CatPrivate() noexcept = default;
CatPrivate::~CatPrivate() noexcept = default;

void CatPrivate::make_cute() noexcept
{
    static constexpr auto ADJECTIVES = std::array { "very", "super", "ultra" };

    const auto random_index = static_cast<std::size_t>(rand()) % ADJECTIVES.size();
    std::puts(fmt::format("Making cat {} cute 😁", ADJECTIVES[random_index]).c_str());
}

CMAKE_PROJECT_TEMPLATE_END_NAMESPACE

CMAKE_PROJECT_TEMPLATE_BEGIN_NAMESPACE

Cat::Cat() noexcept
  : impl_{ new CatPrivate{} }
{
    std::srand(static_cast<std::uint32_t>(time(0)));
}

Cat::~Cat() noexcept = default;

void Cat::make_cute() noexcept
{
    impl_->make_cute();
}

CMAKE_PROJECT_TEMPLATE_END_NAMESPACE
