#include <cmake_project_template/cat.hh>

#include <cstdio>

int main(int, char *[])
{
    std::printf("Hello from %s!\n", CMAKE_PROJECT_TEMPLATE_APPLICATION_ID);

    auto cat = cmake_project_template::Cat{};
    cat.make_cute();

    return 0;
}
