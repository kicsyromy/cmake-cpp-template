set (LVGL_VERSION 0a5a07278ec995938772a2b87526eb571c9c5d36)

include (ffmpeg.cmake)
include (freetype2.cmake)

include (FetchContent)

string (SUBSTRING ${LVGL_VERSION} 0 8 LVGL_COMMIT)
FetchContent_Declare (
    LVGL-${LVGL_COMMIT}
    URL https://github.com/lvgl/lvgl/archive/${LVGL_VERSION}.tar.gz
    URL_HASH SHA256=57c710933bb4b94b70e4109a3c2c76c0f1623faa70330ec627291b0a344626d8
    DOWNLOAD_EXTRACT_TIMESTAMP ON
)

set (LVGL_SRC_DIR ${FETCHCONTENT_BASE_DIR}/lvgl-${LVGL_COMMIT}-src)
set (LVGL_BIN_DIR ${FETCHCONTENT_BASE_DIR}/lvgl-${LVGL_COMMIT}-build)

if (NOT LVGL-${LVGL_COMMIT}_POPULATED)
    FetchContent_Populate (LVGL-${LVGL_COMMIT})
    if (NOT BUILD_SHARED_LIBS)
        add_subdirectory (${LVGL_SRC_DIR} ${LVGL_BIN_DIR} EXCLUDE_FROM_ALL)
    else ()
        add_subdirectory (${LVGL_SRC_DIR} ${LVGL_BIN_DIR})
    endif ()
endif ()

target_compile_definitions (
    lvgl
    PUBLIC
        -DLV_CONF_SKIP=1
        -DLV_COLOR_DEPTH=32
        -DLV_USE_THEME_DEFAULT=0
        -DLV_USE_FREETYPE=1
        -DLV_USE_RLOTTIE=0
        -DLV_USE_LARGE_COORD
        -DLV_DISP_DEF_REFR_PERIOD=1
        -DLV_USE_LOG=1
)

if (${PROJECT_CMAKE_NAMESPACE}_ENABLE_MEMORY_AND_PERF_TRACING)
    target_compile_definitions (
        lvgl
        PUBLIC
            -DLV_USE_ASSERT_MEM_INTEGRITY=1
            -DLV_USE_ASSERT_OBJ=1
            -DLV_USE_ASSERT_STYLE=1
            -DLV_USE_PERF_MONITOR=1
            -DLV_LOG_LEVEL=LV_LOG_LEVEL_INFO
    )
else ()
    target_compile_definitions (
        lvgl PUBLIC -DLV_LOG_LEVEL=LV_LOG_LEVEL_WARN
    )
endif ()

if (FFmpeg_FOUND)
    target_compile_definitions (lvgl PUBLIC -DLV_USE_FFMPEG=1)
    target_link_libraries (lvgl PUBLIC ${PROJECT_ID}::FFmpeg)
endif ()

target_link_libraries (
    lvgl PUBLIC
    ${PROJECT_ID}::FreeType2
)

add_library(
    ${PROJECT_ID}_lvgl INTERFACE
)
add_library (${PROJECT_ID}::LVGL ALIAS ${PROJECT_ID}_lvgl)
target_link_libraries (${PROJECT_ID}_lvgl INTERFACE lvgl)
