set (RLOTTIE_VERSION 875626965959d8e269ca22175c8e1ad190696c43)

find_project_dependency (
    NAME RLottie
    NAMES rlottie
    FIND_PKG_IMPORTED_TARGET rlottie::rlottie
    SET_PKG_CONFIG_TARGET_PROPERTIES
    SET_CMAKE_CONFIG_TARGET_PROPERTIES
)

if (NOT RLottie_FOUND)
    set (RLottie_DIR "RLottie_DIR-NOTFOUND" CACHE PATH "" FORCE)

    include (FetchContent)

    string (SUBSTRING ${RLOTTIE_VERSION} 0 8 RLOTTIE_COMMIT)
    FetchContent_Declare (
        RLOTTIE-${RLOTTIE_COMMIT}
        URL https://github.com/Samsung/rlottie/archive/${RLOTTIE_VERSION}.tar.gz
        URL_HASH SHA256=74b8d5d1867c8868363868fd327bb83b30640fc37d2c9718593fa156eef0941b
        DOWNLOAD_EXTRACT_TIMESTAMP ON
    )

    set (RLOTTIE_SRC_DIR ${FETCHCONTENT_BASE_DIR}/rlottie-${RLOTTIE_COMMIT}-src)
    set (RLOTTIE_BIN_DIR ${FETCHCONTENT_BASE_DIR}/rlottie-${RLOTTIE_COMMIT}-build)

    if (NOT RLOTTIE-${RLOTTIE_COMMIT}_POPULATED)
        FetchContent_Populate (RLOTTIE-${RLOTTIE_COMMIT})
        if (NOT BUILD_SHARED_LIBS)
            add_subdirectory (${RLOTTIE_SRC_DIR} ${RLOTTIE_BIN_DIR} EXCLUDE_FROM_ALL)
        else ()
            add_subdirectory (${RLOTTIE_SRC_DIR} ${RLOTTIE_BIN_DIR})
        endif ()
    endif ()

    add_library(
        ${PROJECT_ID}_rlottie INTERFACE
    )
    add_library (${PROJECT_ID}::RLottie ALIAS ${PROJECT_ID}_rlottie)
    target_link_libraries (${PROJECT_ID}_rlottie INTERFACE rlottie)
endif ()
