set (FMT_VERSION 8.1.1)

find_project_dependency (
    NAME FMT
    NAMES fmt
    FIND_PKG_IMPORTED_TARGET fmt::fmt
    SET_PKG_CONFIG_TARGET_PROPERTIES
    SET_CMAKE_CONFIG_TARGET_PROPERTIES
)

if (NOT FMT_FOUND)
    include (FetchContent)
    FetchContent_Declare (
        FMT-${FMT_VERSION}
        URL https://github.com/fmtlib/fmt/archive/refs/tags/${FMT_VERSION}.tar.gz
        URL_HASH SHA256=3d794d3cf67633b34b2771eb9f073bde87e846e0d395d254df7b211ef1ec7346
        DOWNLOAD_EXTRACT_TIMESTAMP ON
    )

    set (FMT_SRC_DIR ${FETCHCONTENT_BASE_DIR}/fmt-${FMT_VERSION}-src)
    set (FMT_BIN_DIR ${FETCHCONTENT_BASE_DIR}/fmt-${FMT_VERSION}-build)

    set (FMT_DOC OFF)
    set (FMT_INSTALL OFF)
    set (FMT_OS OFF)
    set (FMT_TEST OFF)

    if (NOT FMT-${FMT_VERSION}_POPULATED)
        FetchContent_Populate (FMT-${FMT_VERSION})
        if (NOT BUILD_SHARED_LIBS)
            add_subdirectory (${FMT_SRC_DIR} ${FMT_BIN_DIR} EXCLUDE_FROM_ALL)

            install (
                DIRECTORY ${FMT_SRC_DIR}/include/
                DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/
                FILES_MATCHING PATTERN "*.h"
            )
        else ()
            add_subdirectory (${FMT_SRC_DIR} ${FMT_BIN_DIR})
        endif ()
    endif ()

    add_library (${PROJECT_ID}_fmt INTERFACE)
    add_library (${PROJECT_ID}::FMT ALIAS ${PROJECT_ID}_fmt)
    target_link_libraries (${PROJECT_ID}_fmt INTERFACE fmt::fmt)
endif ()
