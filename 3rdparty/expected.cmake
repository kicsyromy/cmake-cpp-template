set (EXPECTED_VERSION 96d547c03d2feab8db64c53c3744a9b4a7c8f2c5)

find_project_dependency (
    NAME Expected
    NAMES expected tl-expected
    FIND_PKG_IMPORTED_TARGET tl::expected
    SET_PKG_CONFIG_TARGET_PROPERTIES
    SET_CMAKE_CONFIG_TARGET_PROPERTIES
)

if (NOT Expected_FOUND)
    include (FetchContent)
    include (GNUInstallDirs)

    string (SUBSTRING ${EXPECTED_VERSION} 0 8 EXPECTED_COMMIT)
    FetchContent_Declare (
        EXPECTED-${EXPECTED_COMMIT}
        URL https://github.com/TartanLlama/expected/archive/${EXPECTED_VERSION}.tar.gz
        URL_HASH SHA256=64901df1de9a5a3737b331d3e1de146fa6ffb997017368b322c08f45c51b90a7
        DOWNLOAD_EXTRACT_TIMESTAMP ON
    )

    set (EXPECTED_SRC_DIR ${FETCHCONTENT_BASE_DIR}/expected-${EXPECTED_COMMIT}-src)
    set (EXPECTED_BIN_DIR ${FETCHCONTENT_BASE_DIR}/expected-${EXPECTED_COMMIT}-build)

    set (EXPECTED_BUILD_TESTS OFF)
    set (EXPECTED_BUILD_PACKAGE_DEB OFF)

    if (NOT EXPECTED-${EXPECTED_COMMIT}_POPULATED)
        FetchContent_Populate (EXPECTED-${EXPECTED_COMMIT})
        add_subdirectory (${EXPECTED_SRC_DIR} ${EXPECTED_BIN_DIR} EXCLUDE_FROM_ALL)
    endif ()

    add_library(
        ${PROJECT_ID}_expected INTERFACE
    )
    add_library (${PROJECT_ID}::Expected ALIAS ${PROJECT_ID}_expected)
    target_link_libraries (${PROJECT_ID}_expected INTERFACE tl::expected)

    install (
        DIRECTORY ${EXPECTED_SRC_DIR}/include/
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/
        FILES_MATCHING PATTERN "*.hpp"
    )
endif ()
