set (FREETYPE2_VERSION 2.11.1)

find_project_dependency (
    NAME FreeType2
    NAMES freetype2 Freetype2 Freetype freetype
    FIND_PKG_IMPORTED_TARGET freetype
    SET_PKG_CONFIG_TARGET_PROPERTIES
    SET_CMAKE_CONFIG_TARGET_PROPERTIES
)

if (NOT FreeType2_FOUND)
    include (FetchContent)
    FetchContent_Declare (
        FREETYPE2-${FREETYPE2_VERSION}
        URL https://download.savannah.gnu.org/releases/freetype/freetype-${FREETYPE2_VERSION}.tar.gz
        URL_HASH SHA256=f8db94d307e9c54961b39a1cc799a67d46681480696ed72ecf78d4473770f09b
        DOWNLOAD_EXTRACT_TIMESTAMP ON
    )

    set (FREETYPE2_SRC_DIR ${FETCHCONTENT_BASE_DIR}/freetype2-${FREETYPE2_VERSION}-src)
    set (FREETYPE2_BIN_DIR ${FETCHCONTENT_BASE_DIR}/freetype2-${FREETYPE2_VERSION}-build)

    set (DISABLE_FORCE_DEBUG_POSTFIX ON)

    if (NOT FREETYPE2-${FREETYPE2_VERSION}_POPULATED)
        FetchContent_Populate (FREETYPE2-${FREETYPE2_VERSION})
        if (NOT BUILD_SHARED_LIBS)
            add_subdirectory (${FREETYPE2_SRC_DIR} ${FREETYPE2_BIN_DIR} EXCLUDE_FROM_ALL)
        else ()
            add_subdirectory (${FREETYPE2_SRC_DIR} ${FREETYPE2_BIN_DIR})
        endif ()
    endif ()

    add_library(
        ${PROJECT_ID}_freetype2 INTERFACE
    )
    add_library (${PROJECT_ID}::FreeType2 ALIAS ${PROJECT_ID}_freetype2)
    target_link_libraries (${PROJECT_ID}_freetype2 INTERFACE freetype)
endif ()
