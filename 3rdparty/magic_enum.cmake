set (MAGIC_ENUM_VERSION 64bedded2a9645470a26f3be0942bf5dad0f53e8)

find_project_dependency (
    NAME MAGIC_ENUM
    NAMES magic_enum
    FIND_PKG_IMPORTED_TARGET magic_enum::magic_enum
    SET_PKG_CONFIG_TARGET_PROPERTIES
    SET_CMAKE_CONFIG_TARGET_PROPERTIES
)

if (NOT MAGIC_ENUM_FOUND)
    include (FetchContent)

    string (SUBSTRING ${MAGIC_ENUM_VERSION} 0 8 MAGIC_ENUM_COMMIT)
    FetchContent_Declare (
        MAGIC_ENUM-${MAGIC_ENUM_COMMIT}
        URL https://github.com/Neargye/magic_enum/archive/${MAGIC_ENUM_VERSION}.tar.gz
        URL_HASH SHA256=a63d62dceda03252b3c3872516dee1fa7540f6ff930b7840d2eb1be65b712a19
        DOWNLOAD_EXTRACT_TIMESTAMP ON
    )

    if (NOT MAGIC_ENUM-${MAGIC_ENUM_COMMIT}_POPULATED)
        FetchContent_Populate (MAGIC_ENUM-${MAGIC_ENUM_COMMIT})
    endif ()

    add_library (${PROJECT_ID}_magic_enum INTERFACE)
    add_library (${PROJECT_ID}::MagicEnum ALIAS ${PROJECT_ID}_magic_enum)
    target_include_directories(${PROJECT_ID}_magic_enum SYSTEM INTERFACE ${FETCHCONTENT_BASE_DIR}/magic_enum-${MAGIC_ENUM_COMMIT}-src/include)
endif ()
