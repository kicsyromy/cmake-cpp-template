set (NAPPGUI_VERSION 1.2.2)

find_project_dependency (
    NAME NAppGUI
    NAMES nappgui NAppGui Nappgui NAppGUI
    COMPONENTS sewer osbs core geom2d draw2d osgui gui osapp inet nlib
    FIND_PKG_IMPORTED_TARGET NAppGUI::NAppGUI
    SET_PKG_CONFIG_TARGET_PROPERTIES
    SET_CMAKE_CONFIG_TARGET_PROPERTIES
)

if (NOT NAppGUI_FOUND)
    set (NAPPGUI_PREFIX_DIR ${CMAKE_BINARY_DIR}/_deps/nappgui-${NAPPGUI_VERSION})
    set (NAPPGUI_SRC_DIR ${CMAKE_BINARY_DIR}/_deps/nappgui-${NAPPGUI_VERSION}/src/nappgui/src)
    set (NAPPGUI_BIN_DIR ${CMAKE_BINARY_DIR}/_deps/nappgui-${NAPPGUI_VERSION}/src/nappgui-build)

    set (
        NAPPGUI_MODULES
        "gui;osapp;osgui;draw2d;core;osbs;sewer;geom2d;inet;utils/nlib"
    )

    include (ExternalProject)
    ExternalProject_Add (
        nappgui
        PREFIX ${NAPPGUI_PREFIX_DIR}
        URL https://github.com/frang75/nappgui_src/archive/refs/tags/v${NAPPGUI_VERSION}.tar.gz
        URL_HASH SHA256=c53a02c278ad57d02b81b2ac513e15fcbfe904cdcd490a543bb2cde086a2e253
        DOWNLOAD_EXTRACT_TIMESTAMP ON
        CONFIGURE_HANDLED_BY_BUILD ON
        SOURCE_SUBDIR src
        INSTALL_COMMAND ""
        CMAKE_ARGS
            -DCMAKE_VS_PLATFORM_TOOLSET=v143
            $<$<CONFIG:Release,MinSizeRel>:-DCMAKE_BUILD_CONFIG=Release>
            $<$<CONFIG:Debug>:-DCMAKE_BUILD_CONFIG=Debug>
            $<$<CONFIG:RelWithDebInfo>:-DCMAKE_BUILD_CONFIG=ReleaseWithAssert>
        BUILD_BYPRODUCTS
            # Set the libraries as byproducts of the ExternalProject to help CMake set up the dependency chain
            ${NAPPGUI_BIN_DIR}/gui/${CMAKE_STATIC_LIBRARY_PREFIX}gui${CMAKE_STATIC_LIBRARY_SUFFIX}
            ${NAPPGUI_BIN_DIR}/osapp/${CMAKE_STATIC_LIBRARY_PREFIX}osapp${CMAKE_STATIC_LIBRARY_SUFFIX}
            ${NAPPGUI_BIN_DIR}/osgui/${CMAKE_STATIC_LIBRARY_PREFIX}osgui${CMAKE_STATIC_LIBRARY_SUFFIX}
            ${NAPPGUI_BIN_DIR}/draw2d/${CMAKE_STATIC_LIBRARY_PREFIX}draw2d${CMAKE_STATIC_LIBRARY_SUFFIX}
            ${NAPPGUI_BIN_DIR}/core/${CMAKE_STATIC_LIBRARY_PREFIX}core${CMAKE_STATIC_LIBRARY_SUFFIX}
            ${NAPPGUI_BIN_DIR}/osbs/${CMAKE_STATIC_LIBRARY_PREFIX}osbs${CMAKE_STATIC_LIBRARY_SUFFIX}
            ${NAPPGUI_BIN_DIR}/sewer/${CMAKE_STATIC_LIBRARY_PREFIX}sewer${CMAKE_STATIC_LIBRARY_SUFFIX}
            ${NAPPGUI_BIN_DIR}/geom2d/${CMAKE_STATIC_LIBRARY_PREFIX}geom2d${CMAKE_STATIC_LIBRARY_SUFFIX}
            ${NAPPGUI_BIN_DIR}/inet/${CMAKE_STATIC_LIBRARY_PREFIX}inet${CMAKE_STATIC_LIBRARY_SUFFIX}
            ${NAPPGUI_BIN_DIR}/utils/nlib/${CMAKE_STATIC_LIBRARY_PREFIX}nlib${CMAKE_STATIC_LIBRARY_SUFFIX}
    )

    add_library (${PROJECT_ID}_nappgui INTERFACE)
    add_library (${PROJECT_ID}::NAppGUI ALIAS ${PROJECT_ID}_nappgui)
    add_dependencies (${PROJECT_ID}_nappgui nappgui)

    foreach (MODULE IN LISTS NAPPGUI_MODULES)
        cmake_path (GET MODULE FILENAME library_name)

        add_library (nappgui_${library_name} STATIC IMPORTED GLOBAL)
        set_target_properties (
            nappgui_${library_name} PROPERTIES
            IMPORTED_LOCATION
                ${NAPPGUI_BIN_DIR}/${MODULE}/${CMAKE_STATIC_LIBRARY_PREFIX}${library_name}${CMAKE_STATIC_LIBRARY_SUFFIX}
            INTERFACE_INCLUDE_DIRECTORIES
                ${NAPPGUI_SRC_DIR}/${MODULE}
        )

        target_link_libraries (
            ${PROJECT_ID}_nappgui
            INTERFACE
                nappgui_${library_name}
        )
    endforeach()

    if (${CMAKE_SYSTEM_NAME} MATCHES "Linux")
        find_package (CURL QUIET REQUIRED)
        find_package (Threads)

        find_package (PkgConfig QUIET REQUIRED)
        pkg_check_modules (GTK3 QUIET REQUIRED gtk+-3.0 IMPORTED_TARGET)

        target_link_libraries (
            ${PROJECT_ID}_nappgui
            INTERFACE
                Threads::Threads
                PkgConfig::GTK3
                CURL::libcurl
        )
    elseif (${CMAKE_SYSTEM_NAME} MATCHES "Windows")
        target_compile_definitions (
            ${PROJECT_ID}_nappgui
            INTERFACE
                -D_WINDOWS
                -D__WINDOWS__
        )
    endif()

    # TODO: Figure out how to set this based on build type (multi-config generators are problematic, in particular)
    target_compile_definitions (${PROJECT_ID}_nappgui INTERFACE -DCMAKE_RELEASEWITHASSERT)
endif ()
