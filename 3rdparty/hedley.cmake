set (HEDLEY_VERSION 8fb0604a8095f6c907378cc3f0391520ae843f6f)

find_project_dependency (
    NAME HEDLEY
    NAMES hedley
    SET_PKG_CONFIG_TARGET_PROPERTIES
    SET_CMAKE_CONFIG_TARGET_PROPERTIES
)

if (NOT HEDLEY_FOUND)
    include (FetchContent)
    
    string (SUBSTRING ${HEDLEY_VERSION} 0 8 HEDLEY_COMMIT)
    FetchContent_Declare (
        HEDLEY-${HEDLEY_COMMIT}
        URL https://github.com/nemequ/hedley/archive/${HEDLEY_VERSION}.tar.gz
        URL_HASH SHA256=aea6cb90997d67b4706166a761207c6bb833e201937c46ee8dd170b6e8cc6730
        DOWNLOAD_EXTRACT_TIMESTAMP ON
    )

    if (NOT HEDLEY-${HEDLEY_COMMIT}_POPULATED)
        FetchContent_Populate (HEDLEY-${HEDLEY_COMMIT})
    endif ()

    add_library (${PROJECT_ID}_hedley INTERFACE)
    add_library (${PROJECT_ID}::Hedley ALIAS ${PROJECT_ID}_hedley)
    target_include_directories(${PROJECT_ID}_hedley SYSTEM INTERFACE ${FETCHCONTENT_BASE_DIR}/hedley-${HEDLEY_COMMIT}-src)
endif ()
