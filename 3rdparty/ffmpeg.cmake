find_project_dependency (
    NAME FFmpeg
    NAMES ffmpeg FFMPEG
    COMPONENTS avformat avcodec avutil swscale
    FIND_PKG_IMPORTED_TARGET FFmpeg::FFmpeg
    SET_PKG_CONFIG_TARGET_PROPERTIES
    SET_CMAKE_CONFIG_TARGET_PROPERTIES
)
