set (MSGSL_VERSION 383723676cd548d615159701ac3d050f8dd1e128)

find_project_dependency (
    NAME Microsoft.GSL
    NAMES msgsl gsl microsoftgsl ms-gsl
    FIND_PKG_IMPORTED_TARGET Microsoft.GSL::GSL
    SET_PKG_CONFIG_TARGET_PROPERTIES
    SET_CMAKE_CONFIG_TARGET_PROPERTIES
)

if (NOT Microsoft.GSL_FOUND)
    include (FetchContent)

    string (SUBSTRING ${MSGSL_VERSION} 0 8 MSGSL_COMMIT)
    FetchContent_Declare (
        MSGSL-${MSGSL_COMMIT}
        URL https://github.com/microsoft/GSL/archive/${MSGSL_VERSION}.tar.gz
        URL_HASH SHA256=ecfe918708f0f01d5e163be57a4d5b66c2bf45b8dca945fb83265d66d1a16ebb
        DOWNLOAD_EXTRACT_TIMESTAMP ON
    )

    set (MSGSL_SRC_DIR ${FETCHCONTENT_BASE_DIR}/msgsl-${MSGSL_COMMIT}-src)
    set (MSGSL_BIN_DIR ${FETCHCONTENT_BASE_DIR}/msgsl-${MSGSL_COMMIT}-build)

    if (NOT MSGSL-${MSGSL_COMMIT}_POPULATED)
        FetchContent_Populate (MSGSL-${MSGSL_COMMIT})
        add_subdirectory (${MSGSL_SRC_DIR} ${MSGSL_BIN_DIR} EXCLUDE_FROM_ALL)
    endif ()

    add_library (${PROJECT_ID}_msgsl INTERFACE)
    add_library (${PROJECT_ID}::Microsoft.GSL ALIAS ${PROJECT_ID}_msgsl)
    target_link_libraries (${PROJECT_ID}_msgsl INTERFACE Microsoft.GSL::GSL)
endif ()
