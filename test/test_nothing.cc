#include <gtest/gtest.h>

#include <vector>

TEST(Nothing, always_pass)
{
    ASSERT_TRUE(true);
}

TEST(Nothing, maybe_fail)
{
    int *p = nullptr;
    {
        auto v = std::vector<int>{ 3, 4, 5, 6, 7, 8};
        p = &v[2];
    }
    
    ASSERT_TRUE(*p == 5);
}
