function (generate_pkg_config)
    set (
        one_value_args
        TARGET
        PC_FILE_IN
        ADDITIONAL_DEPENDENCIES
    )

    cmake_parse_arguments (
        GPC "" "${one_value_args}" "" ${ARGN}
    )

    set (DEPENDENCY_LIST "")
    function (collect_dependencies target)
        get_target_property (IDEPS ${target} INTERFACE_LINK_LIBRARIES)
        get_target_property (DEPS  ${target} LINK_LIBRARIES)

        list (APPEND DEPS ${IDEPS})
        list (REMOVE_DUPLICATES DEPS)

        foreach (dependency IN LISTS DEPS)
            if (TARGET ${dependency})
                get_target_property (alias ${dependency} ALIASED_TARGET)
                if (TARGET ${alias})
                    set (dependency ${alias})
                endif ()

                list(APPEND DEPENDENCY_LIST ${dependency})
                collect_dependencies (${dependency})
            endif ()
        endforeach ()
        set (DEPENDENCY_LIST ${DEPENDENCY_LIST} PARENT_SCOPE)
    endfunction()

    collect_dependencies(${GPC_TARGET})
    list(REMOVE_DUPLICATES DEPENDENCY_LIST)

    foreach (dep IN LISTS DEPENDENCY_LIST)
        if (TARGET ${dep})
            get_target_property (${dep}_REQUIRES ${dep} PKG_CONFIG_REQUIRES)
            if (NOT (${${dep}_REQUIRES} MATCHES ".+-NOTFOUND"))
                list (APPEND PKG_CONFIG_REQUIRES ${${dep}_REQUIRES})
            endif ()

            get_target_property (${dep}_CFLAGS ${dep} PKG_CONFIG_CFLAGS)
            if (NOT (${${dep}_CFLAGS} MATCHES ".+-NOTFOUND"))
                list (APPEND PKG_CONFIG_CFLAGS ${${dep}_CFLAGS})
            endif ()

            get_target_property (${dep}_LIBS ${dep} PKG_CONFIG_LIBS)
            if (NOT (${${dep}_LIBS} MATCHES ".+-NOTFOUND"))
                list (APPEND PKG_CONFIG_LIBS ${${dep}_LIBS})
            endif ()
        endif ()
    endforeach ()

    set (PKG_CONFIG_EXEC_PREFIX ${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_LIBDIR})
    set (PKG_CONFIG_LIBDIR ${CMAKE_INSTALL_LIBDIR})
    if (WIN32 AND BUILD_SHARED_LIBS)
        set (PKG_CONFIG_LIBDIR ${CMAKE_INSTALL_BINDIR})
    endif ()
    set (PKG_CONFIG_INCLUDEDIR ${CMAKE_INSTALL_INCLUDEDIR})
    set (PKG_CONFIG_LIBRARY_NAME ${GPC_TARGET})

    if (PKG_CONFIG_REQUIRES)
        list (REMOVE_DUPLICATES PKG_CONFIG_REQUIRES)
        string (REPLACE ";" " " PKG_CONFIG_REQUIRES " ${PKG_CONFIG_REQUIRES}")
    endif ()

    if (PKG_CONFIG_CFLAGS)
        list (REMOVE_DUPLICATES PKG_CONFIG_CFLAGS)
        string (REPLACE ";" " " PKG_CONFIG_CFLAGS " ${PKG_CONFIG_CFLAGS}")
    endif ()

    if (PKG_CONFIG_LIBS)
        list (REMOVE_DUPLICATES PKG_CONFIG_LIBS)
        string (REPLACE ";" " " PKG_CONFIG_LIBS " ${PKG_CONFIG_LIBS}")
    endif ()

    configure_file (${GPC_PC_FILE_IN} ${GPC_TARGET}.pc @ONLY)
    install (
        FILES ${CMAKE_CURRENT_BINARY_DIR}/${PKG_CONFIG_LIBRARY_NAME}.pc
        DESTINATION ${CMAKE_INSTALL_LIBDIR}/pkgconfig
    )
endfunction ()
