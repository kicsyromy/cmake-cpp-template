#pragma once

// Inspired by: https://devblogs.microsoft.com/cppblog/clear-functional-c-documentation-with-sphinx-breathe-doxygen-cmake/

#include <cmake_project_template/config.hh>

#include <memory>

CMAKE_PROJECT_TEMPLATE_BEGIN_PRIVATE_NAMESPACE

class CatPrivate;

CMAKE_PROJECT_TEMPLATE_END_NAMESPACE

CMAKE_PROJECT_TEMPLATE_BEGIN_NAMESPACE

/**
 * A fluffy feeline
 */
class Cat
{
private:
    using CatPrivate = ::CMAKE_PROJECT_TEMPLATE_PRIVATE_NAMESPACE::CatPrivate;

public:
    /**
     * Adopts an orphan cat
     */
    Cat() noexcept;

    /**
     * Gives the cat e new home
     */
    ~Cat() noexcept;

public:
    /**
     * Make this cat look super cute
     */
    void make_cute() noexcept;

private:
    std::unique_ptr<CatPrivate> impl_{};
};

CMAKE_PROJECT_TEMPLATE_END_NAMESPACE
