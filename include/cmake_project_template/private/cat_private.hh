#pragma once

#include <cmake_project_template/config.hh>

CMAKE_PROJECT_TEMPLATE_BEGIN_PRIVATE_NAMESPACE

class CatPrivate
{
public:
    CatPrivate() noexcept;
    ~CatPrivate() noexcept;

public:
    void make_cute() noexcept;
};

CMAKE_PROJECT_TEMPLATE_END_NAMESPACE
